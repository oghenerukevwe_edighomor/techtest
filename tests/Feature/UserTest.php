<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use DatabaseMigrations;

    public function testCreateUser()
    {
        $user = User::factory()->make();
     
        //When submits  request to create endpoint
       $this->post('users.store',$user->toArray()); // your route to create user
     
        //It gets stored in the database
        $this->assertEquals(1, User::all()->count());
    }




    public function testGetUsers()
    {
        //Given we have task in the database
       $user = User::factory()->create();

        //When user visit the page
        $response = $this->get('/');
        
        //He should be able to read the task
        $response->assertSee($user);
    }
    

    public function testSingleUser()
     {
    //Given we have user in the database
    $user = User::factory()->create();
    //When user visit the user's URI
    $response = $this->get('/user/'.$user->id);
    //He can see the user details
    $response->assertSee($user->title)
        ->assertSee($user->email);
}




    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }


    public function testUpdateUser()
    {
    $user = User::factory()->create();
 
    $user->name = "Updated Title";
 
    $this->post('updateUser/'.$user->id, $user->toArray()); // your route to update user
    //The user should be updated in the database.
 
    $this->assertDatabaseHas('users',['id'=> $user->id , 'name' => 'Updated Title']);
    }


    public function testDeleteUser()
    {
    $user = User::factory()->create();
 
    $this->post('delete-user/'.$user->id); // your route to delete user
    //The user should be deleted from the database.

    $this->assertDatabaseMissing('users',['id'=> $user->id]);
    }



}
