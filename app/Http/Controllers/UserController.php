<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

use App\Converter\Facade\Currency;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = User::latest()->paginate('10');

        return view('welcome', compact('user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $inputVal= $request->all();
        //
        $validatedData = $request->validate([
            'name' => 'required|unique:users|max:255',
            'email' => 'required|email',
            'amount' => 'required|numeric',
            'currency' => 'required|in:GBP,EUR,USD',
        ]);
        $user = User::create($request->all() + ['password' => 'password']);


    
        return redirect('/')->with('status', 'User Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
           $user = User::findOrFail($id);

           return view('profile', compact('user'));




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);

        return view('edit', compact('user'));
    

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'amount' => 'required|numeric',
            'currency' => 'required|in:GBP,EUR,USD',
        ]);
        $user = User::findOrFail($id);
        $user->update($request->except('_method', '_token'));
        return redirect('/')->with('status', 'Update Successful!');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/')->with('status', 'Record Deleted!');



    }
       
    public function converter(Request $request, User $user)
        {
          $options = $request->rate_option;
          if($options == 'local' || $options == ''){

          $converted = Currency::from($user->currency)->to($request->currency)->amount($user->amount)->convert();
            
          }else{
         $converted = Currency::createExternalRatesApiDriver()->from($user->currency)->to($request->currency)->amount($user->amount)->convert();
          
        }        
          $user->currency = $request->currency;
          $user->amount = round($converted, 2);
    
         
    
          return response()->json($user);
        }











}
