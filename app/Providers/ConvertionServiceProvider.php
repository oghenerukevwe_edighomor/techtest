<?php

namespace App\Providers;

use App\Converter\ConvertionManager;
use Illuminate\Support\ServiceProvider;

class ConvertionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton('currencyrate', function ($app) {
            return new ConvertionManager($app);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['currencyrate'];
    }

}
