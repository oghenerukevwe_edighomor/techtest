<?php

namespace App\Converter\Contracts;

interface Currency
{
    
    public function convert();
}
