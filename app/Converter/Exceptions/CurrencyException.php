<?php

namespace App\Converter\Exceptions;

use Exception;

class CurrencyException extends Exception
{
  public function __construct($message)
  {
      parent::__construct(
          $this->message = $message
      );
  }
}
