<?php

namespace App\Converter\Drivers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;

class ExternalRatesApiDriver extends Driver
{

   
    public function convert()
    {
        if ($this->fromCurrency === $this->toCurrency) {
            return $this->amount;
        } else {
            $response = Http::get('https://api.exchangeratesapi.io/latest?symbols=' . $this->toCurrency .'&format=1&base=' . $this->fromCurrency);

            if ($response->ok()) {
                $rates =  $response['rates'];
                $rate = (Arr::flatten($rates));

            
                $result = $this->amount * $rate[0];
                return $result;
            } else {
                return;
            }
        }
    }
}
