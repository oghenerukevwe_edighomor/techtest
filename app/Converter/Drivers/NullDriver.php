<?php

namespace App\Converter\Drivers;

class NullDriver extends Driver
{
    
    public function convert()
    {
        return [];
    }
}
