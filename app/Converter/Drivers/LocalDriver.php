<?php

namespace App\Converter\Drivers;

class LocalDriver extends Driver
{
    
    public function convert()
    {
        if ($this->fromCurrency === $this->toCurrency) {
            return $this->amount;
        } else {
            $rate = config('currencyrate.' . $this->fromCurrency . '.' . $this->toCurrency);
        
            $result = $this->amount * $rate;
            return $result;
        }
    }
}
