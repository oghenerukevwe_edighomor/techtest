<?php

namespace App\Converter\Drivers;

use Illuminate\Support\Arr;
use App\Converter\Contracts\Currency;
use App\Converter\Exceptions\CurrencyException;

abstract class Driver implements Currency
{
    /**
     * The starting currency.
     *
     * @var string
    */
    protected $fromCurrency;

    /**
     * The currency to convert to.
     *
     * @var string
    */
    protected $toCurrency;

    /**
     * The value of the currency.
     *
     * @var string
    */
    protected $amount;

    /**
     * {@inheritdoc}
     */
    //abstract public function convert();

    /**
     * Set the currency from value.
     *
     * @param string  $fromCurrency
     *
     * @throws \App\Components\Currency\Exceptions\CurrencyException
     *
     * @return $this
    */

    public function from(string $fromCurrency)
    {
        throw_if(is_null($fromCurrency), CurrencyException::class, 'Currency from value cannot be empty');

        $this->fromCurrency = $fromCurrency;

        return $this;
    }

    /**
     * Set the currency to value.
     *
     * @param string $toCurrency
     *
     * @throws \App\Components\Currency\Exceptions\CurrencyException
     *
     * @return $this
    */

    public function to(string $toCurrency)
    {
        throw_if(empty($toCurrency), CurrencyException::class, 'Currency to value cannot be empty');

        $this->toCurrency = $toCurrency;

        return $this;
    }

    /**
     * Set the currency amount.
     *
     * @param string  $amount
     *
     * @throws \App\Components\Currency\Exceptions\CurrencyException
     *
     * @return $this
    */

    public function amount(string $amount)
    {
        throw_if(empty($amount), CurrencyException::class, 'Currency amount cannot be empty');

        $this->amount = $amount;

        return $this;
    }
}
