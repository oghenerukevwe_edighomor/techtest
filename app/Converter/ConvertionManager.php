<?php

namespace App\Converter;

use Illuminate\Support\Manager;
use App\Converter\Drivers\NullDriver;
use App\Converter\Drivers\LocalDriver;
use App\Converter\Drivers\ExternalRatesApiDriver;

class ConvertionManager extends Manager
{
    // public function channel($name = null)
    // {
    //     return $this->driver($name);
    // }


    public function getDefaultDriver()
    {
        
        return $this->config->get('currencyrate.default');

        
    }

    public function createExternalRatesApiDriver()
    {
        return new ExternalRatesApiDriver();
    }

    public function createNullDriver()
    {
        return new NullDriver;
    }

    public function createLocalDriver()
    {
        return new LocalDriver();
    }
}
















