@extends('layouts.app')
@section('content')


            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           {{$user->name}} 
                            <small>Update Record</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="/">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i>Update User
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->


                <div class="row">
      @if($errors->any())
        <div class="alert alert-danger">
          @foreach ($errors->all() as $error)
            {{ $error }} <br/>
          @endforeach
        </div>
      @endif
        <div class="col-lg-12">
          <form method="POST" action="{{ route('users.update', $user->id) }}">
            {!! csrf_field() !!}
            {{ method_field('PATCH') }}
            <div class="form-row">
              <div class="form-group col-md-6">
                <div class="input-group">
                  <input type="text" class="form-control" id="name" name="name" value="{{ $user->name}}" placeholder="Name">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <div class="input-group">
                  <input type="text" class="form-control" id="email" name="email" value="{{ $user->email}}"  placeholder="Email">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <div class="input-group">
                  <input type="text" class="form-control" id="amount" name="amount" value="{{ $user->amount}}" placeholder="Amount">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <div class="input-group">
                  <select class="form-control" id="currency" name="currency">
                    <option  value="{{ $user->currency}}">{{ $user->currency}}</option>
                    <option value="GBP">GBP</option>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
          </form>
        </div>
      </div>
    </div>
  </div>

            </div>
            <!-- /.container-fluid -->

@endsection
        





 