@extends('layouts.app')
@section('content')


            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            {{$user->name}}
                            <small>Details</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="/">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> info
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->


                <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th >Email</th>
                  <th >Currency</th>
                  <th >Amount</th>
                  <th style="width: 150px">Convert Currency</th>
                  <th>Convertion Type</th>
                </tr>

                 <tr>
                 <td></td>
                 <td col="5">
                  {{$user->name}} </td>
                <td>{{$user->email}}</td>
                <td>{{$user->currency}}</td>
                <td>{{$user->amount}}</td>
                <td>  
                <form method="GET" action="/user/{{ $user->id }}">
                    <div class="col-lg-12 py-1">
                        {{ csrf_field() }}
                        <select class="form-control" id="currency" name="currency">
                        <option value="GBP" {{$user->currency == 'GBP'  ? 'selected' : ''}}>GBP</option>
                        <option value="USD" {{$user->currency == 'USD'  ? 'selected' : ''}}>USD</option>
                        <option value="EUR" {{$user->currency == 'EUR'  ? 'selected' : ''}}>EUR</option>
                        </select>
                    </div>
               </td>
                <td><div class="form-check form-check-inline">
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="rate_option" id="inlineRadio1" value="local">
                    <label class="form-check-label" for="inlineRadio1">Use Local Convertion Rate (Driver) </label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="rate_option" id="inlineRadio2" value="external">
                    <label class="form-check-label" for="inlineRadio2">Use External Convertion Rate (Driver)</label>
                    </div></td><td>
                
                <div class="col-lg-12 py-1">
            <button type="submit" class="btn btn-primary">Convert</button>
          </div>
        </form></td>
                
                </tr>

                </table>

            </div>
            <!-- /.container-fluid -->

@endsection
        





 