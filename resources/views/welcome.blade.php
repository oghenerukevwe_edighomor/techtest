@extends('layouts.app')
@section('content')


            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome 
                            <small>Users</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="/">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Users
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
               @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
                 
                <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th >Email</th>
                  <th >Currency</th>
                  <th >Amount</th>
                  <th style="width: 50px"></th>
                  <th style="width: 50px"></th>
                  <th style="width: 50px"></th>
                </tr>

                 <tr>
                  {{-- <td></td>
                 <td col="5">
                  No Record Found!
                <td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> --}}
                </tr>

                 @foreach($user as $key=>$users)
                 <tr>
                 <td>{{ $key+1 }}</td>
                 <td col="3">{{ $users->name}}</td>
                <td>{{ $users->email}}</td>
                <td>{{ $users->currency}}</td>
                <td>{{ $users->amount}}</td>
                <td> <div class="btn-group">
                      <a href="{{ route('users.show' , $users->id) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                          </div></td>
                <td><div class="btn-group">
                      <a href="{{ route('users.edit' , $users->id) }}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i> </a>
                          </div></td>
                <td> <form  role="form" method="POST" action="{{ route('users.destroy' , $users->id) }}">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                      
                   </form>  </td>
                </tr>
                @endforeach
                </table>

            </div>
            <!-- /.container-fluid -->

@endsection
        





 