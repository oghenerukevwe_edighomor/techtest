@extends('layouts.app')
@section('content')


            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome 
                            <small>Add Users</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="/">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Add Users
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->


     <div class="row">
      @if($errors->any())
        <div class="alert alert-danger">
          @foreach ($errors->all() as $error)
            {{ $error }} <br/>
          @endforeach
        </div>
      @endif
        <div class="col-lg-12">
          <form method="post" action="{{ route('users.store') }}">
            {!! csrf_field() !!}
            <div class="form-row">
              <div class="form-group col-md-6">
                <div class="input-group">
                  <input type="text" class="form-control" id="name" name="name"  placeholder="Name">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <div class="input-group">
                  <input type="text" class="form-control" id="email" name="email"  placeholder="Email">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <div class="input-group">
                  <input type="text" class="form-control" id="amount" name="amount"  placeholder="Amount">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <div class="input-group">
                  <select class="form-control" id="currency" name="currency">
                    <option value="GBP">GBP</option>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>

            </div>
            <!-- /.container-fluid -->

@endsection
        





 