<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/',[UserController::class, 'index']);

Route::resources([
    'users' => UserController::class,
]);

Route::get('/user/{user}', [UserController::class, 'converter']);






// Route::get('/',[UserController::class, 'index']);
// Route::get('/create',[UserController::class, 'create']);
// Route::get('/user/{id}', [UserController::class, 'show']);
// Route::post('/addUser', [UserController::class, 'store']);
// Route::patch('/updateUser/{id}', [UserController::class, 'update']);
// Route::post('/DeleteUser/{id}', [UserController::class, 'destroy']);


