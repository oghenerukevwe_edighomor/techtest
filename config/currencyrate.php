<?php


return [



    'default' => env('CURRENCY_DRIVER', 'local'),


  /*
    |--------------------------------------------------------------------------
    | The local Currency Rates
    |--------------------------------------------------------------------------
    |
    | These are the rates the local currency driver.
    |
    */
    'GBP' => [
        'USD' => '1.3',
        'EUR' => '1.1',
    ],
    'EUR' => [
        'GBP' => '0.9',
        'USD' => '1.2',
    ],
    'USD' => [
        'GBP' => '0.7',
        'EUR' => '0.8',
    ],







];